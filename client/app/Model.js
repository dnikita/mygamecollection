function Model() {
  this.subs = [];
  this.val = [];
}

Model.prototype.subscribe = function (s) {
  this.subs.push(s)
};

Model.prototype.update = function (val) {
  this.val = val;
  this.subs.forEach(function (s) {
    s(val)
  });
};

module.exports = Model;
