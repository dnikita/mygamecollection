/** @jsx React.DOM */
var React = require('react/addons');
var LandingPage = require('./components/LandingPage');
var global = require('./global');
var Model = require('./Model');

var mountNode = document.getElementById("react-main-mount");

var model = new Model();

function requestUpdate() {
  if (!global.token) return model.update([]);
  $.get('/api/games?access_token=' + global.token)
    .then(function (newData) {
      model.update(newData);
    }, function () {
      model.update([]);
    });
}

function onLogin(data) {
  global.token = data.id;
  global.userId = data.userId;
  requestUpdate()
}

global.on('login', onLogin);
global.on('data:change', requestUpdate);

function render() {
  React.render(
    <LandingPage data={model.val}/>,
    mountNode
  );
}

model.subscribe(render);
render();
