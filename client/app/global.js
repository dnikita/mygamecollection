/**
 * Created by ndyumin on 28.08.2015.
 */

function Global() {
  this.channels = {};
}

Global.prototype.on = function(channel, clb) {
  this.channels[channel] = this.channels[channel] || [];
  this.channels[channel].push(clb);
};

Global.prototype.trigger = function(channel, data) {
  if (this.channels[channel]) {
    this.channels[channel].forEach(function(clb) {
      clb(data);
    });
  }
};

module.exports = new Global();
