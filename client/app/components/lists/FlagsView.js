/** @jsx React.DOM */

var React = require('react/addons');

var Flags = React.createClass({
  render: function() {
    var languages = this.props.data || [];
    return <div>{languages.map(function(lang, i) {
      var norm = (lang || "").toLowerCase();
      return <img key={i} src="https://upload.wikimedia.org/wikipedia/commons/c/ce/Transparent.gif" className={"flag flag-"+norm} alt={lang} />
    })}</div>;
  }
});

module.exports = Flags;
