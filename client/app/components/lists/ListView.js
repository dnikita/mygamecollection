/** @jsx React.DOM */

var React = require('react/addons');

var CompoundCell = React.createClass({
  render: function () {
    return <div>
      {this.props.data.map(function (p, i) {
        return <div key={i}>{p}</div>;
      })}
    </div>
  }
});

module.exports = CompoundCell;
