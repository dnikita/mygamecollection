/** @jsx React.DOM */

var React = require('react/addons');
var Flags = require('../lists/FlagsView');
var CompoundCell = require('../lists/ListView');

var DetailedView = React.createClass({
  render: function () {
    var model = this.props.model;
    return <div className="jumbotron">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h1>{model.name}</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <h3>Description</h3>
            {model.description}
          </div>
        </div>
        <div className="row">
          <div className="col-md-3 col-sm-6 col-xs-12">
            <h3>Platforms</h3>
            <CompoundCell data={model.platforms}/>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <h3>Genres</h3>
            <CompoundCell data={model.genre}/>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <h3>Rating</h3>
            {model.rating}
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <h3>Languages</h3>
            <Flags data={model.languages}/>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <button type="button" className="btn btn-default" onClick={this.props.onCloseClick}>Close</button>
          </div>
        </div>
      </div>
    </div>
  }
});

module.exports = DetailedView;
