/** @jsx React.DOM */
/**
 * Created by ndyumin on 28.08.2015.
 */

var React = require('react/addons');
var global = require('../../global');

var SigninForm = React.createClass({
  _onLogin: function(data) {
    global.trigger(this.props.action, data);
    this.setState({
      error: ''
    });
    $('#signin').modal('hide');
  },
  _onFailure:function(e) {
    this.setState({
      error: e.responseJSON.error.message
    });
  },
  signin: function () {
    var data = ({
      email: $('input#emailField').val(),
      password: $('input#passwordField').val()
    });

    $.ajax({
      url: 'api/Users/' + (this.props.action === 'login' ? 'login' : ''),
      contentType: "application/json",
      type: 'POST',
      data: JSON.stringify(data)
    }).then(this._onLogin).fail(this._onFailure);
  },
  getInitialState: function() {
    return {
      error:''
    };
  },
  componentWillReceiveProps: function () {
    this.setState({ error: '' });
  },
  render: function () {
    return (
      <div className="modal fade" id="signin" tabIndex="-1" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
              <h4 className="modal-title">Sign in</h4>
            </div>
            <div className="modal-body">
              {this.state.error ? <div className="alert alert-danger" role="alert">{this.state.error}</div> : ''}
              <div className="input-group">
                <span className="input-group-addon" id="basic-addon1">email</span>
                <input type="text" id="emailField" className="form-control" placeholder="Username"
                       aria-describedby="basic-addon1" defaultValue="a@a.aa"/>
              </div>

              <div className="input-group">
                <span className="input-group-addon" id="basic-addon2">password</span>
                <input type="password" id="passwordField" className="form-control" placeholder="Password"
                       aria-describedby="basic-addon2" defaultValue="a"/>
              </div>
            </div>

            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="button" className="btn btn-primary" onClick={this.signin}>Login</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
});


module.exports = SigninForm;
