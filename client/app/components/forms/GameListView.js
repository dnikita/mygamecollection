/** @jsx React.DOM */

var React = require('react/addons');
var global = require('../../global');
var EditForm = require('./GameEditor');
var GameViewer = require('./GameViewer');
var ListView = require('../lists/ListView');
var FlagsView = require('../lists/FlagsView');

function getEmptyModel() {
  return {
    name: '',
    genre: [],
    languages: [],
    platforms: [],
    rating: 0,
    description: ''
  }
}

var Table = React.createClass({
  _onSuccess: function () {
    global.trigger('data:change');
  },
  _onClick: function (e) {
    var $target = $(e.target);
    var action = $target.attr('data-action');
    var row = $target.closest('[data-index]');
    var index = row.attr('data-index');
    function getResponseHandler(ctx, action) {
      return function(data) {
        if (this.isMounted()) {
          this.setState({
            action: action,
            gameModel: data
          });
        }
      }.bind(ctx);
    }
    if (index != null) {
      switch (action) {
        case undefined:
          $.get('api/games/'+ index +'?access_token=' + global.token)
            .then(getResponseHandler(this, 'view'));
              break;
        case 'edit':
          $.get('api/games/'+ index +'?access_token=' + global.token)
            .then(getResponseHandler(this, action));
          $('#newgame').modal('show');
          break;
        case 'remove':
          this.setState({
            action: 'none',
            gameModel: getEmptyModel()
          });
          $.ajax({
            url: 'api/games/' + index + '?access_token=' + global.token,
            type: 'DELETE'
          }).then(this._onSuccess);
          break;
      }
    }
  },
  _onCreateClick: function() {
    this.setState({
      action: 'create',
      gameModel: getEmptyModel()
    });
    $('#newgame').modal('show');
  },
  _onFilterChange: function (e) {
    var $target = $(e.target),
      name = $target.attr('data-field'),
      value = $target.val(),
      update = {};
    update[name] = value;
    this.setState(update);
  },
  onDetailedCloseClick: function() {
    this.setState({
      action: 'none',
      gameModel: getEmptyModel()
    });
  },
  getInitialState: function() {
    return {
      action: 'none',
      gameModel: getEmptyModel()
    };
  },
  render: function () {
    var nameFilter = this.state.nameFilter,
        platformFilter = this.state.platformFilter;

    var data = this.props.data || [];
    if (nameFilter) {
      data = data.filter(function(item) {
        return item.name.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1;
      });
    }
    if (platformFilter) {
      data = data.filter(function (item) {
        return item.platforms
          .some(function (platform) {
            return platform.toLowerCase().indexOf(platformFilter.toLowerCase()) !== -1
          });
      });
    }

    return <div onClick={this._onClick} className="table-responsive">
      <button type="button" className="btn btn-default" onClick={this._onCreateClick}>new game</button>
      {this.state.action === 'view' ? <GameViewer model={this.state.gameModel} onCloseClick={this.onDetailedCloseClick}/> : ''}
      <div className="input-group">
        <span className="input-group-addon" id="nameFilterAddon">Name</span>
        <input type="text" id="nameFilter" className="form-control" placeholder="filter by name"
               aria-describedby="nameFilterAddon" onChange={this._onFilterChange} data-field="nameFilter" value={nameFilter} />
        <span className="input-group-addon" id="platformFilterAddon">Platform</span>
        <input type="text" id="platformFilter" className="form-control" placeholder="filter by platform"
               aria-describedby="platformFilterAddon" onChange={this._onFilterChange} data-field="platformFilter" value={platformFilter} />
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <td>Title</td>
            <td>Genre</td>
            <td>Platform</td>
            <td>Languages</td>
          </tr>
        </thead>
        <tbody>
        {data.map(function (row) {
          return <tr data-index={row.id} key={row.id}>
            <td>{row.name}</td>
            <td><ListView data={row.genre}/></td>
            <td><ListView data={row.platforms}/></td>
            <td><FlagsView data={row.languages}/></td>
            <td className="glyphicon glyphicon-edit" data-action="edit"></td>
            <td className="glyphicon glyphicon-remove" data-action="remove"></td>
          </tr>
        })}
        </tbody>
      </table>
      <EditForm action={this.state.action} model={this.state.gameModel}/>
    </div>
  }
});

module.exports = Table;
