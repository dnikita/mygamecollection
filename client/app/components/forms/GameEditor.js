/** @jsx React.DOM */
/**
 * Created by ndyumin on 29.08.2015.
 */

var React = require('react/addons');
var global = require('../../global');


var EditForm = React.createClass({
  _onFailure: function(e) {
    this.setState({
      error: e.responseJSON.error.message
    });
  },
  _onSuccessfulUpdate: function() {
    global.trigger('data:change');
    this.setState({
      error: ''
    });
    $('#newgame').modal('hide');
  },
  _onSaveClick: function() {
    var index = this.props.gameId || "",
        verb = this.props.action === 'edit' ? 'PUT' : 'POST';

    $.ajax({
      url: 'api/games/' + index + '?access_token=' + global.token,
      type: verb,
      contentType: "application/json",
      data: JSON.stringify(this.props.model)
    }).then(this._onSuccessfulUpdate, this._onFailure);
  },
  _onChange: function(e) {
    var $target = $(e.target),
        field = $target.attr('data-field'),
        value = $target.val(),
        model = this.props.model;

    if (field) {
      if ($target.attr('data-type') === 'array') {
        if (value !== '') {
          value = value.split(',');
        } else {
          value = null;
        }
      }
      model[field] = value;
    }

    this.setState({ model: model });
  },
  getInitialState: function() {
    return {
      error: ''
    }
  },
  componentWillReceiveProps: function () {
    this.setState({ error: '' });
  },
  render: function() {
    var model = this.props.model;
    return (
      <div className="modal fade" id="newgame" tabIndex="-1" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
              <h4 className="modal-title">{(this.props.action === 'edit' ? "Edit: " : "New game: ") + model.name}</h4>
            </div>
            <div className="modal-body">
              {this.state.error ? <div className="alert alert-danger" role="alert">{this.state.error}</div> : ''}
              <div className="input-group">
                <span className="input-group-addon" id="basic-addon1">name</span>
                <input type="text" id="nameField" className="form-control" placeholder="name"
                       aria-describedby="basic-addon1" onChange={this._onChange} data-field="name" value={model.name} />
              </div>

              <div className="input-group">
                <span className="input-group-addon" id="basic-addon2">platforms (comma separated)</span>
                <input type="text" id="platformsField" className="form-control" placeholder="platforms"
                       aria-describedby="basic-addon2" onChange={this._onChange} data-type="array" data-field="platforms" value={model.platforms} />
              </div>

              <div className="input-group">
                <span className="input-group-addon" id="basic-addon3">languages (comma separated)</span>
                <input type="text" id="languagesField" className="form-control" placeholder="languages"
                       aria-describedby="basic-addon3" onChange={this._onChange} data-type="array" data-field="languages" value={model.languages} />
              </div>

              <div className="input-group">
                <span className="input-group-addon" id="basic-addon4">genres (comma separated)</span>
                <input type="text" id="genreField" className="form-control" placeholder="genres"
                       aria-describedby="basic-addon4" onChange={this._onChange} data-type="array" data-field="genre" value={model.genre} />
              </div>

              <div className="input-group">
                <span className="input-group-addon" id="basic-addon5">rating</span>
                <input type="text" id="ratingField" className="form-control" placeholder="rating"
                       aria-describedby="basic-addon5" onChange={this._onChange} data-field="rating" value={model.rating} />
              </div>

              <div className="input-group">
                <span className="input-group-addon" id="basic-addon2">description</span>
                <textarea id="descriptionField" className="form-control" placeholder="description" rows="5"
                       aria-describedby="basic-addon2" onChange={this._onChange} data-field="description" value={model.description} />
              </div>
            </div>

            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="button" className="btn btn-primary" onClick={this._onSaveClick}>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = EditForm;
