/** @jsx React.DOM */

var React = require('react/addons');
var AuthForm = require('./forms/AuthForm');
var GameListView = require('./forms/GameListView');
var Intro = require('./Intro');
var global = require('../global');

var ReactApp = React.createClass({
  _onSignOutClick: function() {
    global.token = null;
    global.trigger('data:change');
  },
  getInitialState: function() {
    return {
      action: 'login'
    };
  },
  signin: function() {
    this.setState({
      action: 'login'
    });
  },
  signup: function() {
    this.setState({
      action: 'signup'
    });
  },
  render: function () {
    return (
      <div>
        <button type="button" className="btn btn-default" data-toggle="modal" data-target="#signin" onClick={this.signup}>Sign up</button>
        { !global.token ? <button type="button" className="btn btn-default" data-toggle="modal" data-target="#signin" onClick={this.signin}>Sign in</button> : "" }
        { !global.token ? <Intro /> : "" }
        { global.token ? <button type="button" className="btn btn-default" onClick={this._onSignOutClick} >Sign out</button> : "" }
        { global.token ? <GameListView data={this.props.data}/> : "" }
        <AuthForm id="signin" action={this.state.action}/>
      </div>
    )
  }
});

module.exports = ReactApp;
