/** @jsx React.DOM */
var React = require('react/addons');

var Intro = React.createClass({
  render: function () {
    var link = 'http://qrcoder.ru/code/?http%3A%2F%2F178.62.228.80%3A3000%2F&6&0';
    return <div className="jumbotron">
      <div className="container">
        <h1>ReadMe</h1>

        <p>This is a sample game collection solution.</p>
        <h4>Description</h4>
        It consists of a REST Service (Node.js, loopback, express, ejs, React) and Single page application (React,
        jQuery, Bootstrap)

        <h4>REST Service</h4>
        <ul>
          <li><b>POST</b> /api/Users - creates a user</li>
          <li><b>POST</b> /api/Users/login - signs in the user (sends a token back)</li>
          <li><b>GET</b> /api/games - lists the games of the current user</li>
          <li><b>GET</b> /api/games/:id - sends the game by id (if it belongs to the current user)</li>
          <li><b>POST</b> /api/games - creates a game for the current user</li>
          <li><b>PUT</b> /api/games/:id - updates the game for the current user</li>
          <li><b>DELETE</b> /api/games/:id - removes the game for the current user</li>
        </ul>
        <h4>Client</h4>

        Once logged in a user is presented the list of games in his\her collection.<br/>
        A click on a row would bring in a detailed view for the chosen game. <br/>
        Corresponding games could be updated\removed with the icons in the rightmost column of each row.<br/>
        The button "new game" shows a dialog for creating a new game.<br/>
        In case of a failed service request, a message will be displayed.<br/>
        <h4>Conventions</h4>
        <ul>
          <li>Data is not persisted, but on each start there are two default users created: <b>a@a.aa</b> and <b>b@a.aa</b> with the password <b>a</b>.</li>
          <li>Filters are case insensitive</li>
          <li>In addition there are several random entries (games) created for each of them</li>
          <li>Properties that have multiple items (Arrays) are edited as a string where items are separated with commas </li>
          <li>Application designed to be responsive and work on mobile devices, although it was not extensively tested. </li>
        </ul>
        <a href="http://qrcoder.ru" target="_blank"><img src={link} width="198" height="198" border="0" title="QR code" /></a><br/>
        <a href="https://bitbucket.org/dnikita/mygamecollection">https://bitbucket.org/dnikita/mygamecollection</a>
      </div>
    </div>;
  }
});

module.exports = Intro;
