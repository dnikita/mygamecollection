

var loopback = require('loopback');
var boot = require('loopback-boot');
var express = require('express');
var path = require('path');

var app = module.exports = loopback();

app.start = function() {
  return app.listen(function() {
    app.emit('started');
    console.log('Web server listening at: %s', app.get('url'));
  });
};

app.use(express.static(path.join(__dirname, 'client/static')));
app.set('views', path.join(__dirname, '../client/views'));
app.set('view engine', 'ejs');


boot(app, __dirname, function(err) {
  if (err) throw err;

  if (require.main === module)
    app.start();
});
