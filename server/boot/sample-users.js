module.exports = function(app) {

  var GENRES = ['action', 'strategy', 'tbs', 'rpg', 'quest', 'puzzle'];
  var LANG = ['GB', 'RO', 'NO', 'PL', 'EE', 'RU', 'FR', 'DE', 'FI', 'ES'];
  var PLATFORMS = ['Windows', 'Linux', 'iOS', 'Android', 'SteamOS'];
  var CHARS = 'abcdefghijklmnopqrstuvwxyz0123456789              '.split('');
  var NOUNS = ['Life', 'Box', 'Pirate', 'Warror', 'Reign', 'Satellite', 'League', 'Piece', 'Company', 'Legacy'];
  var ADJS = ['Mad', 'Super', 'Old', 'Mega', 'One', 'Dangerous', 'Grand'];

  var adjGenerator = randomArrayElement.bind(null, ADJS);
  var nounGenerator = randomArrayElement.bind(null, NOUNS);

  function randomArrayElement(arr) {
    return arr[(Math.random()*arr.length)|0];
  }

  function randomString(l) {
    var s = '';
    for (var i =0; i < l; i += 1) {
      s += randomArrayElement(CHARS);
    }
    return s;
  }

  function randomArray(minLength, maxLength, generator) {
    if (minLength > maxLength) throw new Error('invalid arguments values');

    var result = [],
        l = (Math.random()*(maxLength-minLength + 1))|0 + minLength;

    for (var i = 0; i < l; i+= 1) {
      var el = generator();
      while (result.indexOf(el) !== -1) {
        el = generator();
      }
      result.push(el);
    }
    return result;
  }

  function generateSysReq() {
    return {
      os: randomArrayElement(PLATFORMS),
      cpu: (Math.random()*1000 + 1000)|0,
      hdd: Math.ceil(Math.random()*40),
      ram: Math.ceil(Math.random()*8)
    };
  }

  function randomNounPhrase() {
    return randomArray(0, 2,
      function () {
        return [].concat(
          randomArray(0, 1, adjGenerator),
          randomArray(1, 2, nounGenerator)
        ).join(' ');
      }
    ).join(' of a ');
  }

  function generateGame() {
    return {
      name: randomNounPhrase(),
      genre: randomArray(2, 5, randomArrayElement.bind(null, GENRES)),
      languages: randomArray(2, 5, randomArrayElement.bind(null, LANG)),
      platforms: randomArray(1, 3, randomArrayElement.bind(null, PLATFORMS)),
      requirements: randomArray(1, 3, generateSysReq),
      description: randomString(200),
      rating: (Math.random()*10)|0
    };
  }

  function onUserCreated(err, user) {
    if (err) throw new Error('error while creating a user');

    var games = randomArray(15, 30, generateGame);
    games.forEach(function(game) {
      game.userId = user.id;
      app.models.Game.create(game)
    });
  }

  function createUser(user) {
    app.models.User.create(user, onUserCreated)
  }

  var users = [
    {
      "email": "a@a.aa",
      "password": "a"
    },
    {
      "email": "b@a.aa",
      "password": "a"
    }
  ];

  users.forEach(createUser);
};
