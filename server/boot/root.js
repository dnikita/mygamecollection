require("node-jsx").install({harmony: true});

module.exports = function(server) {
  var React = require('react/addons'),
    LandingPage = React.createFactory(require('../../client/app/components/LandingPage'));

  var router = server.loopback.Router();


  router.get('/status', server.loopback.status());
  router.get('/', function(req, res) {
    res.render('index.ejs', {
      reactOutput: React.renderToString(new LandingPage({}))
    });

  });
  server.use(router);
};
