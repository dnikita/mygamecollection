var gulp = require('gulp'),
  browserify = require('gulp-browserify');

gulp.task('scripts', function () {

  gulp.src(['client/app/main.js'])
    .pipe(browserify({
      debug: true,
      transform: ['reactify']
    }))
    .pipe(gulp.dest('./client/static/'));

});

gulp.task('default', ['scripts']);
