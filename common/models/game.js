var loopback = require('loopback');

module.exports = function(Game) {
  /**
   * only allow users access their games
   */
  Game.observe('access', function(ctx, next) {
    var userId = loopback.getCurrentContext().get('accessToken').userId;
    ctx.query.where = ctx.query.where || {};
    ctx.query.where.userId = userId;
    next();
  });
  /**
   * mark each Game entry with a userId
   */
  Game.observe('persist', function(ctx, next) {
    ctx.data.userId = ctx.data.userId || loopback.getCurrentContext().get('accessToken').userId;
    next();
  });
};
